import data from "./data.js";

const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const rowStart = $(".row-start");
const rowEnd = $(".row-end");
const totalEntries = $(".total-entries");
const btnPage = $(".btn-page");
const btnPrev = $(".btn-prev");
const btnNext = $(".btn-next");

// >>>>>>>>>> Render table heading <<<<<<<<<<

const tableHeading = (list) => {
  const tr = $("thead tr");
  list.map(
    (heading) =>
      (tr.innerHTML += `<th class="${
        heading.slice(0, 1).toLowerCase() + heading.slice(1)
      }">
            ${heading}
            <span class="sort">
                <span class="asc">&#9650</span>
                <span class="desc">&#9660</span>
            </span>
        </th>`)
  );
};

const tHeadings = [
  "Name",
  "Position",
  "Office",
  "Extn",
  "Startsdate",
  "Salary",
];

tableHeading(tHeadings);

const initState = {
  querySet: data,
  rowPerPage: 10,
  pageNumber: 1,
};

// render button page number
const showBtnPage = () => {
  btnPage.innerHTML = "";
  const pages = Math.ceil(initState.querySet.length / +entriesPerPage.value);

  if (pages == 0) {
    btnPage.innerHTML += `<button class="active" value="1">1</button>`;
  } else {
    for (let i = 1; i <= pages; i++) {
      if (i === 1) {
        btnPage.innerHTML += `<button class="active" value="${i}">${i}</button>`;
      } else {
        btnPage.innerHTML += `<button value="${i}">${i}</button>`;
      }
    }
  }
};

// display entries footer
const showEntries = () => {
  const entries = initState.querySet.length;
  const start =
    initState.pageNumber * initState.rowPerPage - initState.rowPerPage + 1;
  const end = start + initState.rowPerPage;

  start > entries ? (rowStart.innerHTML = 1) : (rowStart.innerHTML = start);
  end > entries ? (rowEnd.innerHTML = entries) : (rowEnd.innerHTML = end);
  totalEntries.innerHTML = entries;
};

// >>>>>>>>>> Change rows per page <<<<<<<<<<
const entriesPerPage = $("#entries");
entriesPerPage.addEventListener("change", () => {
  initState.rowPerPage = +entriesPerPage.value;
  // showBtnPage();
  render();
});

// >>>>>>>>>> Render dataset into table <<<<<<<<<<

const render = (dataSet = initState.querySet, renderBtn = true) => {
  const tbody = $("tbody");
  tbody.innerHTML = "";
  dataSet.slice(0, initState.rowPerPage).map(
    (obj) =>
      (tbody.innerHTML += `<tr>
            <td>${obj[0]}</td>
            <td>${obj[1]}</td>
            <td>${obj[2]}</td>
            <td>${obj[3]}</td>
            <td>${obj[4]}</td>
            <td>${obj[5]}</td>
        </tr>`)
  );
  if (renderBtn) showBtnPage();
  // showBtnPage();
  showEntries();
};
render();
// showBtnPage();

// >>>>>>>>>> Searching <<<<<<<<<<

const search = $("#search");
const filterTable = (value, data) => {
  const dataFilted = data.filter((items) => {
    const match = items.some((item) =>
      item.toLowerCase().includes(value.toLowerCase())
    );
    return match;
  });
  initState.querySet = dataFilted;
  // showBtnPage();
  render();
};
search.addEventListener("keyup", () => filterTable(search.value, data));

// >>>>>>>>>> Sort <<<<<<<<<<

// Sort asc
const sortAsc = (col, index) => {
  if (col.className == "extn") {
    initState.querySet.sort((a, b) => a[index] - b[index]);
  } else if (col.className == "start") {
    initState.querySet.sort((a, b) => {
      const na = a[index].split("/");
      const nb = b[index].split("/");
      return +na[0] - +nb[0] || +na[1] - +nb[1] || +na[2] - +nb[2];
    });
  } else if (col.className == "salary") {
    initState.querySet.sort((a, b) => {
      const na = +a[index].slice(1).replace(/\,/g, "");
      const nb = +b[index].slice(1).replace(/\,/g, "");
      return na - nb;
    });
  } else {
    initState.querySet.sort((a, b) => {
      if (a[index] < b[index]) return -1;
      if (a[index] > b[index]) return 1;
      return 0;
    });
  }
  // showBtnPage();
  render();
};

// Sort desc
const sortDesc = (col, index) => {
  if (col.className == "extn") {
    initState.querySet.sort((a, b) => b[index] - a[index]);
  } else if (col.className == "start") {
    initState.querySet.sort((a, b) => {
      const na = a[index].split("/");
      const nb = b[index].split("/");
      return +nb[0] - +na[0] || +nb[1] - +na[1] || +nb[2] - +na[2];
    });
  } else if (col.className == "salary") {
    initState.querySet.sort((a, b) => {
      const na = +a[index].slice(1).replace(/\,/g, "");
      const nb = +b[index].slice(1).replace(/\,/g, "");
      return nb - na;
    });
  } else {
    initState.querySet.sort((a, b) => {
      if (a[index] < b[index]) return 1;
      if (a[index] > b[index]) return -1;
      return 0;
    });
  }
  // showBtnPage();
  render();
};

// Sort events

const th = $$("th");
th.forEach((col, index) => {
  let sort = true;
  col.addEventListener("click", () => {
    if (sort) {
      if ($(".sorted") != null) {
        $(".sorted").classList.remove("sorted");
      }
      const dad = col.className;
      $(`.${dad} .asc`).classList.add("sorted");
      sortAsc(col, index);
      sort = false;
    } else {
      if ($(".sorted") != null) {
        $(".sorted").classList.remove("sorted");
      }
      const dad = col.className;
      $(`.${dad} .desc`).classList.add("sorted");
      sortDesc(col, index);
      sort = true;
    }
  });
});

// >>>>>>>>>> Pagination <<<<<<<<<<

const pagination = (page) => {
    const start = (page - 1) * entriesPerPage.value;
    const end = start + +entriesPerPage.value;
    render(data.slice(start, end), false);
};

// Button previous
btnPrev.addEventListener("click", () => {
  pagination(--initState.pageNumber);
});

// Button next
btnNext.addEventListener("click", () => {
  pagination(++initState.pageNumber);
});

// Button page number
const btnPages = $$(".btn-page button");
btnPages.forEach((btn) => {
  btn.addEventListener("click", () => {
    initState.pageNumber = +btn.value;
    $(".active").classList.remove("active");
    btn.classList.add("active");
    pagination(+btn.value);

    if (btn.value == 1) {
      btnPrev.classList.add("unable");
      btnNext.classList.remove("unable");
    } else if (btn.value == 4) {
      btnNext.classList.add("unable");
      btnPrev.classList.remove("unable");
    } else {
      btnPrev.classList.remove("unable");
      btnNext.classList.remove("unable");
    }
  });
});
if (btnPages.length == 1) {
  btnPrev.classList.add("unable");
  btnNext.classList.add("unable");
}
